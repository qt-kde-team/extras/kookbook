kookbook (0.2.1-3) unstable; urgency=medium

  * Minor updates from lintian-brush.
  * Update Homepage field.

 -- Stuart Prescott <stuart@debian.org>  Sun, 29 Jan 2023 19:02:47 +1100

kookbook (0.2.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.4.1, no changes needed.

  [ Stuart Prescott ]
  * Update Standards-Version to 4.5.1 (no changes required).
  * Update to debhelper-compat (= 13).
  * Set build option reproducible=-fixfilepath to prevent dpkg change from
    breaking test suite (Closes: #972308).

 -- Stuart Prescott <stuart@debian.org>  Sat, 09 Jan 2021 00:05:23 +1100

kookbook (0.2.1-1) unstable; urgency=medium

  * New upstream release.
    - drop local copy of krecipes converter.

 -- Stuart Prescott <stuart@debian.org>  Wed, 23 Jan 2019 23:54:56 +1100

kookbook (0.1.0-2) unstable; urgency=medium

  * Add patch to ensure that kookbooktouch is also installed.
  * Include extra dependencies needed for kookbooktouch.
  * Fix upstream URLs to point to kde.org.
  * Don't recommend users store data in ~/.local since the touch client can't
    navigate there in the folder selector.
  * Add missing licence for cmake files to d/copyright.

 -- Stuart Prescott <stuart@debian.org>  Fri, 18 Jan 2019 00:20:17 +1100

kookbook (0.1.0-1) unstable; urgency=medium

  * Initial release.
  * Add krecipes.py converter from upstream git to help users migrate away
    from krecipes and help with the qtwebkit removal.

 -- Stuart Prescott <stuart@debian.org>  Wed, 16 Jan 2019 23:18:33 +1100
